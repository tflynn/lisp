(print "attempting lambda identity:")
(print ((lambda (x) x) 10))

(print "SYMBOL:")
(print (symbol? (quote ass)))
(print (symbol? (list)))
(print (symbol? "ASD!"))
(print (symbol? (lambda () 10)))

(print "MAP:")
(print (map + (list 1 2 3) (list 4 5 6)))
(print (map + (list 1 2) (list 4 5)))
(print (map + (list 1) (list 4)))
(print (map + (list) (list)))
(print (map + (list)))
(print (map car (list (list "a" "b") (list "c" "d") (list "e" "f"))))


(print "CONS:")
(define b (list 1 2 3))
(print b)
(cons 2 b)
(print b)
(define c (cons 2 b))
(print c)
(print b)

(define myl (list 1 2 3))

(print (car myl)) ;1
(print (cadr myl)) ;2
(print (caddr myl)) ;3


(print "CASE:")
(print
 (case 4
       ((list 1 2 3) (quote small))
       ((list 4 5 6) (quote mid))
       ((list 7 8 9) (quote big))
       (else (quote unknown!))))



(print "PUSH-FRONT")
(print (push-front 1 2 3 (list)))
(print (push-front (list 1 2 3)))
(print (push-front 1 2 (list 3 4)))

(print "PUSH-BACK")
(print (push-back (list)))
(print (push-back (list 1 2 3)))
(print (push-back (list) 1 2 3))
(print (push-back (list 1 2 3) 4 5 6))


(print "MEMQ:")


(print (memq (quote a) (list (quote a) (quote b) (quote c))))
(print (memq "b" (list "a" "b" "c")))
(print (memq  "a" (list "b" "c" "d")))
(print (memq 100 (list 102 100 103)))


(print "APPEND")
(print (append (list) (list 1 2 3)))
(print (append (list) (list)))
(print (append (list 1 2 3) (list 4 5 6)))

(print "NEW APP")
(print (append))
(print (append (list 1 2 3)))
(print (append (list 1 2) (list 3 4) (list 5 6)))
(print (append (list) (list 3 4) (list 5 6)))
(print (append (list 1 2) (list) (list 5 6)))
(print (append (list 1 2) (list) (list)))

(print (quote let:))
(let ((a 10))
  (let ((b (+ a 5)) 
	(a 3)
	(c a))
    (print (+ b (+ a c)))))

(print (quote let*:))
(let ((a 10))
  (let* ((b (+ a 5)) 
	(a 3)
	(c a))
    (print (+ b (+ a c)))))


(print (quote letrec:))
(letrec ((fact (lambda (n) (if (eq? n 1) 1 (* n (fact (+ n -1))))))) (print (fact 4)))

(print (quote defing-a-recursive-fn:))
(define fact (lambda (n) (if (eq? n 1) 1 (* n (fact (+ n -1))))))
(print (quote defing-some-vars:))
(define a  10)
(define b 2)
(print (+ a b))

(define pair (lambda (a b) (lambda (f) (f a b))))
(define first (lambda (p) (p (lambda (a b) a))))
(define second (lambda (p) (p (lambda (a b) b))))
(define mypair (pair 10 20))
(print (first mypair))
(print (second mypair))
(define two (lambda (x y) y))
(two (print (quote yo)) (print (quote frayne)))
(print ((if (eq? 1 0) + *) 2 3))
(define while (lambda (test fn post) (if (test) (two (fn) (while test fn post)) (post))))
(print (quote good))

(define ef (lambda () (print 10)))
(ef)
(ef)
(ef)

(while
	(lambda() (two (print (quote continue?)) (eq? (input) 1)))
	(lambda() (print (quote continuing)))
	(lambda() (print (quote done))))

(define n-times (lambda (fn n) (if (eq? n 1) (fn n) (two (fn n) (n-times fn (+ n -1))))))
(n-times (lambda (n) (print (quote hello-my-frayne))) 10)
(n-times (lambda (n) (print (fact n))) 3)
(print (if (eq? 1 0) 1 0 ))
(define q 0)
(print (quote set!:))
(define ctr (let ((a 10)) (lambda () (set! a (+ a 1)) (print a))))
(print (quote trying-ctr:))
(ctr)
(print (quote trying-ctr-next:))
(ctr) ;a comment )()#W95294)
(ctr) ; another comment
;(ctr)
(ctr)
(load "lib/hi.l")
(print "Back to normal")

