(define *macro-debug* #f)

;
;(_ _ _) list of 3
;_ anything
;(_ _ 9) list of two, last element is 9

;(str-eq (11 _ (_ 33)) (11 12 (14 33))) #t
;(str-branch
; a
; ( lambda #t #t (_ 33)) exp1)
; ( (12 _ (_ 34)) exp2)
; ( (13 _ ( (bind c) 35) exp3))

(define-syntax count-terms body
  (length body))

(define prompt
  (lambda (s)
    (display s) (input)))

(print (count-terms pi ya di))
(print "Should have printed 3")

;(dbind (a b c) (list 1 2 3) (print a b c))

(define can-bind
  (lambda (vals vars) ;a is vals, b is vars
;    (print "CAn-BIND COO")
    (cond
     ((null? vars) (null? vals))
     ((symbol? vars) (not (null? vals)))
     ((list? vars)
      (cond ;first, vars is at least 2ele list, vals not list
       ((and (not (null? (cdr vars))) (not (list? vals)))
	(print "In the cool place")
	(and (eq? (car vars) (quote =))
	     (eq? (cadr vars) vals)))
       (else 
;	(print "ELSE!")
	(if (list? vals)
	    (if (not (null? vals))
		(and (list? vals) 
		     (can-bind (car vals) (car vars))
		     (can-bind (cdr vals) (cdr vars))) #f) #f )))))))
  
(print "CAN-BIND")
(print (can-bind (list 1 10) (list (quote a) ))) ;#f
(print (can-bind (list 1) (list (quote a) (quote b)))) ;#f
(print (can-bind (list 1) (list (quote a)))) ;#t
(print (can-bind (list 1 2 3) (list (quote a) (quote b) (quote c)))) ;#t
(print (can-bind (list 1 2 3) (list (quote a) (quote b)))) ;#f
(print (can-bind (list 1 (list 2 3) 4) (list (quote a) (quote b) (quote c)))) ;#t
(print (can-bind (list 1 (list 2 3) 4) 
		 (list (quote a) (list (quote b) (quote c)) (quote d)))) ;#t

(print (can-bind (list 1 (list 2 3) 4)
		 (list (quote a) (list (quote b)) (quote d)))) ;f

(print "TRYING A COOL ONE")

(print (can-bind (list (quote lambda) (list 2 3) 4)
		 (list (list (quote =) (quote magwitties)) (list (quote a) (quote b)) (quote d)))) ;t?

(define *macro-debug* #f)

(print "PFRONT")
(print (push-front 10 (list)))

(define-syntax listify a
  (let ((itm (car a)))
  (cond
   ((null? itm)  (list))
   ((symbol? itm) (list (quote quote) itm))
   ((list? itm)
	(list (quote push-front)
	      (list (quote listify) (car itm))
	      (if (not (null? (cdr itm)))
		  (list (quote listify) (cdr itm))
		(list (quote list))))))))


(print "LISTIFY:")

(print (listify (a  b  (c d e) d e )))
(print "Should have printed ( a b ( c d e) d e)")

(print "CAN-BIND2:")
(define *macro-debug* #f)

(define-syntax can-bind2 (vals vars) ;Macro version of can-bind
  (list (quote can-bind) vals (list (quote listify) vars)))

(print (can-bind (list 1 (list 2 3) 4)
		  (list (quote a) (list (quote b) ) (quote e))))
(print "Should have printed #f")
       
(print (listify (a (b) e)))
(print (listify (a (b c) e)))

(print (can-bind (list 1 (list 2 3) 4)
		  (listify (a (b c) e))))
(print "Should have printed #t")

;(define *macro-debug* #t)

(print (can-bind2 (list 1 (list 2 3) 4)
		  (a (b c)e)))
(print "Should have printed #t")

;(prompt "10")

(print (can-bind2 (list 1 (quote lambd) (list 2 3) 4)
		  (a (= lambd) (b c)e)))

(print "Should have printed #t")

(which can-bind (list 1 (list 12 13) 10 9999)
       ( (list (quote a)) (print "MAGUINCHC"))
       ( (list (quote a) (quote  b)  (quote c)) (print "MAGWIN"))
       ( (list (quote a) (quote b)) (print "MANC"))
       (else (print "KYS")))

(print "Should have printed KYS")

(define-syntax dbind2 (vars vals . rest) ;actually perform a bind. var names, then data.
  (if (null? vars) (cons (quote begin) rest)
      (push-back
       (if (list? (car vars)) ;recurse
	   (list (quote dbind2) 
		 (car vars)
		 (list (quote car) vals));(dbind2 (c vars) (c vals) rec-body)
	 (list
	  (quote let)
	  (list (list (car vars) (list (quote car) vals)))))
       (push-front (quote dbind2)
		   (cdr vars)
		   (list (quote cdr) vals)
		   rest)))) ;(let ((car vars) (var vals)) rec-body)

(print "DBIND tests")
(define cup 1000)
(print (dbind2
	(b c)
	( cdr ( list 1 2 3 ) )
	(+ b c))) ;should be 5
(print "Should have printed 5")

(print (dbind2
	(a b c d)
	(list 1 2 3 4)
	(print "S y a") (+ a (+ b c)))) ;should be 6
(print "Should have printed 6")

(print (dbind2
	(a (b c) d)
	(list 1 (list 2 3) 4)
	(print "P y a") (+ a (+ b c)))) ;should be 6

(print "Should have printed 6")

(define myl (list 1 2 (list 3 (list 4 5) 6 7)))

(print (dbind2
	(a b (c (d e) f g))
	myl
	(print "go crane") (+ a (+ b (+ d (+ e f)))))) ;should be 18
(print "Should have printed 18")

;; (print ( + 1  (car ( (lambda x x) (+ 1 2))))) ;4


;; (print "WHICH")
;; (print
;;  (which = 7
;;        (1 (quote small))
;;        (4 (quote mid))
;;        (9 (quote big))
;;        (else (quote unknown!))))



;(define *macro-debug* #t)
	   
(define-syntax can-bind3 (l . clauses) ;combine bind test (can-bind) with bind exec (dbind)
  (let ((mapped-clauses
	 (push-front (quote cond)
	 (map (lambda (clause)
		(let ((cset (car clause)))
		  (if (eq? cset (quote else))
		      clause
		    (list (list (quote can-bind2) l cset)
			  (push-front (quote dbind2) cset l
				      (cdr clause))))))
		     clauses))))
	  mapped-clauses))

;(cond-bind l
;	   ( (a (b c) d) (print yo))
;          ( (a b) (print wassup))
;          ( (a b c) (print cillum))
;          ( (/ a b) ( (quote \frac{)  a (quote }{) b (quote })))

(define mylo (list 10 (list 30 40 50) 60 70))

(print "CAN-BIND-3") 
(can-bind3 (list 1 (list 12 13 19 ) 10 9999)
	   ( (a)
	     (print "MAGUINCHC"))
	   ( (a (b c d) e f)
	     (print "MACLUSKY" b ))
	   ( (a b c d)
	     (print "MAGWIN") (print b))
	   ( (a b)
	     (print "MANC"))
	   (else (print "KYS")))


(print "CAN-BIND-3") 
(can-bind3 mylo
	   ( (a)
	     (print "MAGUINCHC"))
	   ( (a (b c d) e f)
	     (print "MACLUSKY" b ))
	   ( (a b c d)
	     (print "MAGWIN") (print b))
	   ( (a b)
	     (print "MANC"))
	   (else (print "KYS")))

(print "Walk test:")

(define-syntax defun body
  (let ((a (car body)) ;fn name
	(b (car (cdr body))) ;arg list
	(c (cdr (cdr body)))) ;codez
    (list (quote define)
	  a
	  (cons
	   (quote lambda)
	   (cons b c)))))

(defun walk-if (body)
  (can-bind3 body
	     (( c1 b1 )
	      (list " if (" (walk c1) "){" (walk b1) "}"))
	     (( c1 b1 e1)
	      (list " if (" (walk c1) "){ " (walk b1) ":}else{" (walk e1) ":}"))
	     ))

(defun walk-min (body)
  (can-bind3 body
	     ((a1 a2)
	      (list (walk a1) "-" (walk a2)))))

(defun walk-lt (body)
  (can-bind3 body
	     ((a1 a2)
	      (list (walk a1) "<" (walk a2)))))
(defun walk-mult (body)
  (can-bind3 body
	     ((a1 a2)
	      (list (walk a1) "*" (walk a2)))))

(defun intify a
  (print "Intifying the ")
  (print a)
  (cons
   "int "
   (cons (car a)
	 (if (not (null? (cdr a)))
	     (list "," (intify (cdr a)))
	   (list)))))

(defun walk-defun (body)
  (print "Length: ")
  (print (length body))
  (print "THE CDDR:")
  (print (cddr body))
  (print "THE CDDR2:")
  (print (cddr body))

  (list "int " (car body) "(" (intify (cadr body)) "){" (walk (cddr body)) ":}"))

(define dosts (lambda (stmnt)
  (print "HI@")))

(print "DOING STS")
(dosts (list 1 2 3))

(define first-ele
  (lambda (l)
    (if (not (list? l)) l
      (if (not (null? l))
	  (first-ele (car l))))))

(print (first-ele (list (list (list 1 2 3)))))
(print (first-ele (list (list (list)))))

(define flatten
  (lambda (l)
    (if (null? l) l
      (if (not (list? l)) l
	(if (not (null? car l)) 
	    (if (not (list? (car l)))
		(cons (car l) (flatten (cdr l)))
	      (append (flatten (car l))
		      (flatten (cdr l)))))))))

(print "DEFINED!")

(print (flatten (list 1 (list 2 (list 3 4) 5) 6)))
(prompt "y?")

(defun walk (body)
  (print "walkin teh dog")
  (cond
   ((null? body) body)
   ((list? body)
    (which eq? (car body)
	   ( (quote if)    (walk-if (cdr body)))
	   ( (quote <)     (print "<!") (walk-lt (cdr body)) )
	   ( (quote -)      (print "-!") (walk-min (cdr body)))
	   ( (quote *)     (print "*")(walk-mult (cdr body)))
	   ( (quote defun)  (print "defun")(walk-defun (cdr body)))
	   ( (quote print)  (print "print!")(walk-print (cdr body)))
	   ( (quote let*) (print "let*!") (walk-let* (cdr body)) )
	   (else (print "multiplez") (cons (walk (car body)) (list ":" (walk (cdr body)))))))
   (else
    (which eq? body
	   ((quote #t) "true")
	   ((quote #f) "false")
	   (else body)))))

(define-syntax walk-ext body
  (print (flatten (walk body))))

;	   (print "If of the first type"))
;	     ( ((= if) c1 b1 e1)
;	       (print "If of the second type"))
;	     (else
;	      (print "Something else entirely!"))))


;(define *macro-debug* #t)
(walk-ext
 (defun myfn (a) (if (< a 10) #f #t) (* a b)))


