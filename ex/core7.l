
(print (and)) ;t
(print (and #t)) ;t
(print (and #f)) ;f
(print (and #t #t #t)) ;t
(print (and #t #f)) ;f
(print (and #f #t)) ;f 
(print (and #f #f)) ;f
(print (and 1 2 (list 99 3)))
(print (and 1 #f (list 111)))
