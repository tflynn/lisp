(define cars
  (lambda lists ;lists should be ((l1) (l2) (l3))
    (if (null? lists)
	 (list)
      (cons (caar lists) (apply cars (cdr lists))))))


(print (cars (list 1 2) (list 3 4) (list 5 6)))
