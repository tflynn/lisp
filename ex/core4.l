10

(define repeat-n
  (lambda (n fn)
    (if (< 0 n)
	(begin
	 (fn n)
	 (repeat-n (- n 1) fn)))))

(define sp-ast
  (lambda (ns na)
    (repeat-n ns (lambda (k) (display " ")))
    (repeat-n na (lambda (k) (display "*")))
    (newline)))

(define diamond
  (lambda (nlines)
    (repeat-n nlines
	      (lambda (n)
		(sp-ast n (+ (* (- nlines n) 2) 1))))))


(diamond 10)

;(defstruct mystr ( (int x) (char c) (float y)))
;add this to the structure database

;(mem-var mystr c) ;lookup in structure database

;(mystr 2)
