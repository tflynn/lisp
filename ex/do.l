;;scm dotest.l

;;without printing;
(print "Only printing the end:")

(print (do
 ( (a 0 b)
   (b 1 (+ a b))
   (n 1 (+ n 1)))
 ( (> n 10) b)  ))

(print "Printing each time:")
;;with printing
(do
 ( (a 0 b)
   (b 1 (+ a b))
   (n 1 (+ n 1)))
 ( (> n 10) ) (print b)  )
