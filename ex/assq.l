(define assq
  (lambda (itm l)
    (if (null? l)
	#f
      (if (eq? (caar l) itm)
	  (car l) 
	(assq itm (cdr l))))))
