(define-syntax test n
  (list (quote print) (quote "Fuck that")))

(define-syntax weird-plus n
  (list (quote +) (car n) (car (cdr (cdr n)))))

;when (a) this1 this2 this3
(define-syntax when body
  (let ((thecond (car body)) ;thecond will be a sym_data tree
	(rest (cdr body))) ;rest will be a list of sym_data trees
    (list (quote if) thecond (cons (quote begin) rest))))

;while (a) this1 
;->
;(letrec ((thefn (lambda () (if (a) (body thefn))))) (thefn)
  
(print (weird-plus 1 2 3))
(when (= 1 0) (print "yo") (print "wtf!!"))


(define-syntax empty-fn body
  (list (quote lambda)
	(list)
	(cons (quote begin)
	body)))

(define x 10)
(define dec-print (lambda () (set! x (- x 1)) (print x)))
(letrec ((thefn (lambda () (if (< 0 x) (begin (dec-print) (thefn)))))) (thefn))
	    
(define y 10)
(define dec-printy (lambda () (set! y (- y 1)) (print y)))

(define cool (empty-fn (print "100") (print "SUP WIT DAT")))

(cool)

(define-syntax while body
  (let ((thecond (car body))
	(rest  (cdr body))) ;one statement for now
    (list (quote letrec)
	  (list (list (quote thefn)
		      (list (quote empty-fn)
			    (list (quote if)
				  thecond
				  (list
				   (quote begin)
				   rest
				   (list (quote thefn)))))))
	  (list (quote thefn)))))

(while (< 0 y) dec-printy)

;(defun a b c) -> (define a (lambda b c))
(define-syntax defun body
  (let ((a (car body)) ;fn name
	(b (car (cdr body))) ;arg list
	(c (cdr (cdr body)))) ;codez
    (list (quote define)
	  a
	  (cons
	   (quote lambda)
	   (cons b c)))))

(defun myprint (n) (print 100) (print (+ n 300)))
(myprint 30)
(print "should have print 100, 330")
