(define lst (list))
(do ((i 1 (+ 1 i)))
    ((> i 4))
    (set! lst (cons (lambda () i) lst)))

(print (map (lambda (proc) (proc)) lst))
