(print "cond")
(define thelib (load "lib/hi.l"))
(print (thelib pi))


(print ((load "lib/hi.l") (thecoolestprint 10)))
(print (((load "lib/hi.l") thecoolestprint) 10))
 
(print "NEXT")
;this first evals thecoolestprint in thelib, then it evals 10 in thelib
(print (thelib thecoolestprint 10))

;this evals 'thecoolestprint 10' in thelib
(print (thelib (thecoolestprint 10)))

;this evals 'thecoolestprint' in the lib, then evals thelib
(print ((thelib thecoolestprint) 10));


;for a unary:
;(print (thelib (thecoolestprint)))
;(print ((thelib thecoolestprint)))

;(thelib a) -> (a) in thelib
;(thelib a) -> a in the lib
