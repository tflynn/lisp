package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;

public class syn_procedure extends syntax_val{//representation of macro 
	public syn_procedure(sym_vals _parm, sym_vals _body, Env _env){
		LinkedList<sym_vals> body = new LinkedList<sym_vals>();
		body.add(_body); //single body exp. for syntactic procedures.
		proc = new procedure(_parm,body,_env); //lambda ast -> ast
	}
	public sym_vals fn_val(LinkedList<sym_vals> vals,
												 Env env){ //called when macro is to be expanded
		sym_vals expanded = proc.fn_val(vals,env); //expand macro
		//expanded is the new ast.
	
		Env e = env.is_defined("*macro-debug*");
		if(e != null){
	    if(e.get("*macro-debug*") == lisp.lisp_true){
				System.out.println("BEGIN DBG:");
				System.out.println(expanded.str_val());
				System.out.println("END DBG");
	    }
		}
		//now we evaluate the new ast, and return this.
		return lisp.eval(expanded,env);//eval expansion

	}
	procedure proc;
}
