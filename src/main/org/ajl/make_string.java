package org.ajl;

import java.util.LinkedList;

public class make_string extends fn_vals{
	public sym_vals fn_val(LinkedList<sym_vals> args){
		sym_vals lhs = args.get(0);
		int n = lhs.num_val();
		String str =
			new String(new char[n]).replace("\0"," ");
		return new sym_str(str);
	}
}
