package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;

public class syntax_let extends syntax_val{
  
	public sym_vals fn_val(LinkedList<sym_vals> vals,
												 Env env,String mode){

		ListIterator<sym_vals> listIt =
	    vals.listIterator();

		if(!listIt.hasNext())
	    throw new IllegalStateException
				("Missing binding section in let");
		sym_vals decls = listIt.next(); //get the ((v1 d1) (v2 d2) ... ) part
		if(!listIt.hasNext())
	    throw new IllegalStateException("Missing body of let");
	    
		if(!(decls instanceof sym_list))
	    throw new IllegalStateException("Invalid form of let binding section");

		Env localenv = new Env(env); //localenv for exp
	    
		ListIterator<sym_vals> declsIt = ((sym_list)decls).children.listIterator();
		//	    System.out.println("Got decls children iterator");
	    
		LinkedList<sym_vals> declVals = new LinkedList<sym_vals>();
		LinkedList<String>  declNames = new LinkedList<String>();

		Env usingenv=null;

		if(mode.equals("let*"))
	    usingenv = localenv;
		else if(mode.equals("let"))
	    usingenv = env;
		else if(mode.equals("letrec"))
	    usingenv = localenv;

		//first, evaluate the forms to be stored in vars.
		//depending on the mode, update the env after each or after all.
		while(declsIt.hasNext()){
	    //		System.out.println("Dereferencing decls iter");
	    sym_list adecl = (sym_list)(declsIt.next());
	    //		System.out.println("Getting decl name");
	    sym_data   dnam  = (sym_data)(adecl.children.get(0)); //name of var
	    //		System.out.println("Getting decl body");
	    sym_vals dval = adecl.children.get(1); //def part
	    declNames.add(dnam.glyph);

	    declVals.add( lisp.eval(dval,usingenv) ); 

	    if(mode.equals("let*")){
		    
				usingenv = new Env(usingenv);
				usingenv.put(declNames.getLast(),declVals.getLast()); //insert into local env
				localenv =usingenv;
	    }
	    else if(mode.equals("let"))
				localenv.put(declNames.getLast(),declVals.getLast());
	    else if(mode.equals("letrec"))
				usingenv.put(declNames.getLast(),declVals.getLast());
	
		}
		sym_vals ret=null;
		while(listIt.hasNext())
	    ret = lisp.eval(listIt.next(),localenv); //get body of exp
		return ret; //eval body in local env



	}
}
