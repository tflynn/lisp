package org.ajl;

import java.util.Scanner;
import java.util.LinkedList;
import java.io.File;

public class load extends fn_vals{

	private static String working_dir =".";
	
	public static parser thep = new std_parse();

	public sym_vals fn_val(LinkedList<sym_vals> sv,
												 Env env){
		Env localenv = new Env(env);
		//		System.out.println("LAOD!");
		run_file(sv.getFirst().str_val(),localenv);
		return localenv;
	}
	
	static void run_file(String fname, Env env){
		//make a back up of old dir
		String old_dir=new String(working_dir);
		//construct file object
		
		File the_file;
		if(fname.charAt(0) == '/')
			the_file=new File("/",fname);
		else
			the_file=new File(working_dir,fname); 
		//set working dir to path of just loaded file
		working_dir=the_file.getParent(); 

		String the_contents = read_contents(the_file);

		//		System.out.println("Opening " + fname);
		//	System.out.println("In the working dir "  + old_dir);
		//	System.out.println("New working_dir: " + working_dir);
		lisp.runProg_throw(the_contents,env,thep);
		
		working_dir=new String(old_dir); //reset working dir
	}

	//for future use to load text.
	static String read_contents(String fname) { 
		return read_contents(new File(working_dir,fname));
	}
	
	static String read_contents(File file) {
		String str = "";
		Scanner input;

		//		File f = new File(filePathString);
		//if(!f.exists() or f.isDirectory()) {
		//	str = "Cannot open file!";
		//	return str;
		//}
		
		try {
			input = new Scanner(file);
		} catch (Exception e) {
			System.out.println("load: Can't open " +
												 file.getPath());
			str = "Error opening file";
			return str;
		}

		while (input.hasNextLine())
			str = str + input.nextLine() + '\n';
		return str;
	}

}

			   
