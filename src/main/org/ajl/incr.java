package org.ajl;

import java.util.LinkedList;

public class incr extends fn_vals{    
    public sym_vals fn_val(LinkedList<sym_vals> args){
	return new sym_num( args.getFirst().num_val() + 1);
    }
}
