package org.ajl;

import java.util.Scanner;
import java.util.LinkedList;

public class read extends fn_vals{
	public static std_parse thep;
	
	public read(){
		thep =
			new std_parse( (new Scanner(System.in)).useDelimiter(""));
	}
	
	public sym_vals fn_val(LinkedList<sym_vals> args){
		sym_vals ret=null;
		if(thep.hasNext())
			ret = thep.next();
		
		if(ret == null) //its weird that we have to check for this, but OK...
			return new sym_eof();
		
		return ret;
	}
}
