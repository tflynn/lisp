package org.ajl;

import java.util.LinkedList;

public class symbol extends fn_vals{  //for now, true if symdata
    public sym_vals fn_val(LinkedList<sym_vals> args){
	sym_vals first = args.get(0);
	if(first instanceof sym_data)
	    return lisp.lisp_true;
	return lisp.lisp_false;
    }
}
