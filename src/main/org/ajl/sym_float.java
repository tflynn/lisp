package org.ajl;

public class sym_float extends sym_vals{
    public boolean eq(sym_vals rhs){
	return ((sym_float)rhs).val == val;
    }
    public sym_float(float _val){
	val = _val;
    }
    public float float_val(){ //numeric value of a symbol
	return val;
    }
    public String str_val(){
	return Float.toString(val);
    }

    float val;
}
