package org.ajl;

import java.util.Iterator;
import java.util.Queue;
import java.util.LinkedList;


public class peekiter<T> implements Iterator<T>
{
	private Iterator<T> scan;
	private T next;
	boolean peeked;
	
	public peekiter( Iterator<T> _scan  )
    {
			scan = _scan;
			peeked=false;
	  }
	
	
	public boolean hasNext() //from java docs: "returns true if next() would return an element rather than throwing an exception."
    {
			if(peeked){
        return true;
			}
			else{
				return scan.hasNext();
			}
			
    }

    public T next()
    {
			if(peeked){
				peeked=false;
				return next;
			}
			else{
				return scan.next();
			}
	  }

    public T peek()
    {
			if(!peeked){
				next = scan.next();
				peeked=true;
			}
			return next;
    }

	public void remove(){
		throw new UnsupportedOperationException("Remove not implemented");
	}

}
