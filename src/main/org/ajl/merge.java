package org.ajl;

import java.util.LinkedList;

public class merge extends fn_vals{
	public sym_vals fn_val(LinkedList<sym_vals> args, Env env){
		sym_vals lhs = args.get(0);

		if(args.size() == 1){ //unary - merge with current env.
			if(lhs instanceof Env){
				env.putAll((Env)lhs);
				return env;
			}
			else{
				throw new IllegalStateException("Error: need two environment arguments");
			}
		}
		else{
			sym_vals rhs = args.get(1);
			if((lhs instanceof Env) &&
				 (rhs instanceof Env)){
				((Env)lhs).putAll((Env)rhs);
				return lhs;
			}
			else{
				throw new IllegalStateException("Error: need two environment arguments");
			}
		}
		
	}
}
