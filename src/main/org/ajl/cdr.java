package org.ajl;

import java.util.LinkedList;

public class cdr extends fn_vals{    //get rest
	public sym_vals fn_val(LinkedList<sym_vals> args){
		sym_list arg = (sym_list)args.get(0);
		sym_list ret = new sym_list( new LinkedList<sym_vals>( arg.children));
		ret.children.removeFirst();
		return ret;
	}
}
