package org.ajl;

import java.util.ListIterator;
import java.util.LinkedList;

public class eq extends fn_vals{
    public sym_vals fn_val(LinkedList<sym_vals> args){
	sym_vals lhs = args.get(0);
	sym_vals rhs = args.get(1);
	if(lhs.getClass() != rhs.getClass())
	    return lisp.lisp_false;
	
	return new sym_bool( lhs.eq(rhs) );					
	/*	return new sym_bool( //number equalit
			   args.get(0).num_val() ==
			    args.get(1).num_val()
			    );*/
	
    }
    
    
}
