package org.ajl;

import java.util.ListIterator;
import java.util.LinkedList;

public class sym_data extends sym_vals{
	public sym_data(String _glyph, int _line){
		glyph = _glyph;
		line = _line;
	}
	
	public sym_data(String _glyph){
		this(_glyph,-1);
	}
  
	public boolean eq(sym_vals rhs){
		return ((sym_data)rhs).glyph.equals(glyph);
	}
  
	public String str_val(){
		return glyph;
	}
    
	String glyph;
	int line;
}
