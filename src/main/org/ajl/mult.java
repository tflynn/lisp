package org.ajl;

import java.util.LinkedList;

public class mult extends fn_vals{
    public sym_vals fn_val(LinkedList<sym_vals> args){
	sym_vals lhs = args.get(0);
	sym_vals rhs = args.get(1);
	if((lhs instanceof sym_num) &&
	   (rhs instanceof sym_num))
	   return new sym_num(lhs.num_val() *
			      rhs.num_val());
	else
	    return new sym_float(lhs.float_val() * rhs.float_val());
	
    }
}
