package org.ajl;

public class sym_str extends sym_vals{
	public sym_str(String str){
		ast = str;
	}
	public String str_val(){
		return ast;
	}
	public boolean eq(sym_vals rhs){
		sym_str r = (sym_str)rhs;
		return ast.equals(r.ast);
	}
	String ast;
}
