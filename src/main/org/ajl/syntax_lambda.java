package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;

public class syntax_lambda extends syntax_val{
    public sym_vals fn_val(LinkedList<sym_vals> vals,
			   Env env){

	ListIterator<sym_vals> listIt = vals.listIterator();
	if(!listIt.hasNext())
	    throw new IllegalStateException
		("Missing parameter definition for lambda");
    
	sym_vals parms = listIt.next();
    
	if(!(parms instanceof sym_list) &&
	   !(parms instanceof sym_data))
	    throw new IllegalStateException
		("Invalid form of lambda parameter list");
    
	if(!listIt.hasNext())
	    throw new IllegalStateException
		("Missing body of lambda expression");
    
    
	LinkedList<sym_vals> body = new LinkedList<sym_vals>(); //store list of exprs
	while(listIt.hasNext())
	    body.add(listIt.next());
    
	return new procedure(
			     parms,
			     body,
			     env);
    
    }

}
