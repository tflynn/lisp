package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;

public class syntax_set extends syntax_val{
	public sym_vals fn_val(LinkedList<sym_vals> vals,
												 Env env){

		ListIterator<sym_vals> listIt = vals.listIterator();

		//	    System.out.println("Doing set!");
		if(!listIt.hasNext())
	    throw new IllegalStateException("set!: missing name");
		sym_data name = (sym_data)(listIt.next());
		String name_glyph = name.glyph;
		if(!listIt.hasNext())
	    throw new IllegalStateException("set!: missing expression");
	
		sym_vals val = listIt.next();
		env.find(name_glyph).put(name_glyph,lisp.eval(val,env));
		//	    System.out.println("Completed set!");
		return env.find(name_glyph).get(name_glyph);


	}
}
