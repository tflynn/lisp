package org.ajl;

import java.util.LinkedList;

public class exp extends fn_vals{
    public sym_vals fn_val(LinkedList<sym_vals> args){
	sym_vals lhs = args.get(0);
	return new sym_float((float)Math.exp(lhs.float_val()));
    }
}
