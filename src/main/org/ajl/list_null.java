package org.ajl;

import java.util.LinkedList;

public class list_null extends fn_vals{    //get rest
    public sym_vals fn_val(LinkedList<sym_vals> args){
	sym_vals first = args.get(0);
	if(first instanceof sym_list)
	    if( ((sym_list)first).children.size() == 0)
		return lisp.lisp_true;
	return lisp.lisp_false;
    }
}
