package org.ajl;

import java.util.LinkedList;

public class apply extends fn_vals{
	public sym_vals fn_val(LinkedList<sym_vals> args){
		if(args.size() < 2)
	    throw new IllegalStateException
				("apply requires at least 2 arguments.");
		if(! (args.get(0) instanceof fn_vals))
	    throw new IllegalStateException
				("Invalid 1st argument to apply, should be a function");
		fn_vals f = (fn_vals)args.get(0);
		if(!(args.get(1) instanceof sym_list))
	    throw new IllegalStateException
				("Invalid 2nd argument to apply, should be a list");
		sym_list al= (sym_list)args.get(1);
		return f.fn_val(al.children);
	}
}
