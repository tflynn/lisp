package org.ajl;

import java.util.LinkedList;

public class string_ref extends fn_vals{
	public sym_vals fn_val(LinkedList<sym_vals> args){
		sym_vals lhs = args.get(0);
		sym_vals rhs = args.get(1);
		return new
			sym_str(lhs.str_val().substring(rhs.num_val(),
																			rhs.num_val()+1));
	}
}
