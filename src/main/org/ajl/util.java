package org.ajl;
	
class util {
	public static boolean is_new_line(char c){
		if(c == '\r')
	    return true;
		if(c == '\n')
	    return true;
		return false;
	}
	
	
	
	public static boolean is_dquote(char c){
		if(c == '"')
	    return true;
		return false;
	}
	
	public static boolean is_white_space(char c){
		if (is_new_line(c))
	    return true;
		if( c == ' ')
	    return true;
		if(c == '\t')
	    return true;
		return false;
	}
	
	//returns n where str[n] == '\n' or str[n] == '\r' or n = str.length()
	public static int until_new_line(int strt, String str){
		int ret=strt;
		while( ret < str.length() &&
					 !is_new_line(str.charAt(ret) ) ) ret++;
		return ret;
	}

}
