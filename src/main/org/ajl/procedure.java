package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;

public class procedure extends fn_vals{ //lambda. type of a user-defined function
    
	public procedure(sym_vals _parms,
									 LinkedList<sym_vals> _body,
									 Env _env){
		parms = _parms; body = _body; env = _env;
	}
	
	public sym_vals fn_val(LinkedList<sym_vals> args){
		ListIterator<sym_vals> argsIt = args.listIterator();

		Env localenv = new Env(env);

		if(parms instanceof sym_list){ //explicit list of parms (a,b . c)
	    ListIterator<sym_vals> parmsIt = 
				((sym_list)parms).children.listIterator();
	    while(argsIt.hasNext()){ //bind each paramter in the localenv.
				sym_data parm_name = (sym_data)parmsIt.next();
				if(!(parm_name.glyph.equals("."))){
					localenv.put(parm_name.glyph,
											 argsIt.next());
				}
				else{//rest implicit
					sym_data rest_name = (sym_data)parmsIt.next(); //name next
					LinkedList<sym_vals> rest_parms =new LinkedList<sym_vals>();
		  
					while(argsIt.hasNext()) //get rest of args
						rest_parms.add(argsIt.next());

					localenv.put(rest_name.glyph, //bind parameter to list
											 new sym_list(rest_parms));

				}
		    
	    }
		}
		else{//implicit list n
	    sym_list plist = new sym_list(args);
	    localenv.put( ((sym_data)parms).glyph, //bind parameter to list
										plist);
		}
		//	System.out.println("Calling eval from lambda");
		//	System.out.println("Will exec the sequence:");
		/*for(astnode ast : body)
			System.out.println(" Lambda ele: " + ast.to_str());*/
		return lisp.eval( body, localenv);
	}
	sym_vals parms; //list of param names
	LinkedList<sym_vals> body;
	Env env;
}
