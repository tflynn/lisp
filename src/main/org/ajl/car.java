package org.ajl;

import java.util.LinkedList;

public class car extends fn_vals{    //get fist
    public sym_vals fn_val(LinkedList<sym_vals> args){
			if(args.size() < 1)
				throw new IllegalStateException
				("car  requires a list as an argument but no arguments were given.");
			
			sym_list arg = (sym_list)args.get(0);
			if(arg.children.size() < 1)
				throw new IllegalStateException
					("car requires a list with at least one element but the argument passed was not of this type.");
			return arg.children.getFirst();
    }
}
