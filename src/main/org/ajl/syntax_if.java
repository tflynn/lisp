package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;

public class syntax_if extends syntax_val{
	public sym_vals fn_val(LinkedList<sym_vals> vals, Env env){
		ListIterator<sym_vals> listIt = vals.listIterator();

		if(!listIt.hasNext())
	    throw new IllegalStateException("If: missing test");
		sym_vals test   = listIt.next();
		if(!listIt.hasNext())
	    throw new IllegalStateException("If: missing consequence");
		sym_vals conseq = listIt.next();
		sym_vals alt = listIt.hasNext()?listIt.next():null;
		sym_vals test_res = lisp.eval(test,env);
		if(test_res.bool_val() == true)
	    return lisp.eval(conseq,env);
		else
	    return (alt==null)?lisp.lisp_false:lisp.eval(alt,env);
	


	}
}

//
//fn: Val -> Val (+, eq, cons, car, *)
//cr: Data -> Val (if, lambda, define, let,...)
//sy: Data -> Data
