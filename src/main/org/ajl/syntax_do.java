package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;

public class syntax_do extends syntax_val{

	ListIterator<sym_vals> getSideEffects(LinkedList<sym_vals> vals){
		ListIterator<sym_vals> listIt = vals.listIterator();
		listIt.next();
		listIt.next();
		return listIt;
	}
	
	public sym_vals fn_val(LinkedList<sym_vals> vals, Env env){
		
		/* vals should be a list of the form
			 ( v
			   t
         e1 e2 ...
				 )
				 where v is a list like
				 ( (var1 init1 step1)
				   (var2 init2 step2)
					 ... )
				 t is a list like
				 ( test expr1 expr2 ...)
		*/
		
		ListIterator<sym_vals> listIt = vals.listIterator();

		if(!listIt.hasNext())
	    throw new IllegalStateException("If: missing vars");
		
		sym_vals vars   = listIt.next(); // ( (v1 i1 s1), (v2, i2, s2) , ... )

		if(!listIt.hasNext())
	    throw new IllegalStateException("If: missing test");
		
		sym_list test = (sym_list)listIt.next(); // (test , expr1 , expr2, ...)
		
		ListIterator<sym_vals> testit = test.children.listIterator();

		sym_vals cond = testit.next(); //cond is e.g. (> n 10)

		//listit now points to the side effect commands.
		//make copy for later.

		Env localenv = new Env(env); //local environment 

		ListIterator<sym_vals> varsIt = ((sym_list)vars).children.listIterator();

		//evaluate all the inits in old context (env) and place into new (localenv)
		while( varsIt.hasNext() ){
			sym_list adecl = (sym_list)(varsIt.next());
	    sym_data dnam  = (sym_data)(adecl.children.get(0)); //name of var
	    sym_vals dval = adecl.children.get(1); //init part
		 
			localenv.put(dnam.glyph,lisp.eval(dval, env));
		}
		
		//the env is now set up. begin loop

		while( lisp.eval( cond, localenv).bool_val() != true){ //eval the condition
			
			//begin by evaluating the side effects.
			ListIterator<sym_vals> sideeffects = getSideEffects(vals);
			while(sideeffects.hasNext())
				lisp.eval(sideeffects.next(), localenv);
			
			
			//prepare the new env
			Env localenv2 = new Env(localenv); //localenv for exp

			varsIt = ((sym_list)vars).children.listIterator();
			
			//update the env all the inits
			while( varsIt.hasNext() ){
				sym_list adecl = (sym_list)(varsIt.next());
				sym_data dnam  = (sym_data)(adecl.children.get(0)); //name of var
				sym_vals dval = adecl.children.get(2); //init part
				localenv2.put(dnam.glyph, lisp.eval(dval,localenv));
			}
			localenv = localenv2;			
		}
		
		sym_vals ret = null;
		while(testit.hasNext())
			ret = lisp.eval(testit.next(), localenv);
		return ret;
		
	}
}

