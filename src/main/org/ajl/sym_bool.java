package org.ajl;

public class sym_bool extends sym_vals{
    public boolean eq(sym_vals rhs){
	return ((sym_bool)rhs).val == val;
    }
    public sym_bool(boolean b){
	val = b;
    }
    public boolean bool_val(){
	return val;
    }
    public String str_val(){
	return val?"#t":"#f";
    }
    boolean val;
}
