package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;

public class newline extends fn_vals{//no newline
    public sym_vals fn_val(LinkedList<sym_vals> args){
	String str="";
	if(args.size() > 0)
	    throw new IllegalStateException("Too many arguments to newline");
	System.out.println();
	return new sym_num(0);
    }
}
