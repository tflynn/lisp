package org.ajl;

import java.util.LinkedList;

public class string_append extends fn_vals{
	public sym_vals fn_val(LinkedList<sym_vals> args){
		String ret="";
		for(sym_vals arg:args)
			ret = ret+arg.str_val();
		return new sym_str(ret);
	}
}
