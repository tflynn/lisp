package org.ajl;

import java.util.LinkedList;

public class log extends fn_vals{
    public sym_vals fn_val(LinkedList<sym_vals> args){
	sym_vals lhs = args.get(0);
	return new sym_float((float)Math.log(lhs.float_val() ));
    }
}
