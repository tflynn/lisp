package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;

public class syntax_define extends syntax_val{
	public sym_vals fn_val(LinkedList<sym_vals> vals,
												 Env env){

		ListIterator<sym_vals> listIt = vals.listIterator();
		if(!listIt.hasNext())
	    throw new IllegalStateException("define: missing name");
		sym_data name = (sym_data)(listIt.next());
		if(!listIt.hasNext())
	    throw new IllegalStateException("define: missing definition body");
		sym_vals val  = listIt.next();
		//	System.out.println("Defining " + name.glyph);
		env.put(name.glyph,lisp.eval(val,env)); //evaluate next data, assign to glyph.
		//the type of name.glyph is whatever eval returns.
		return env.get(name.glyph);//define at this level
	}

}
