package org.ajl;

import java.util.LinkedList;

public abstract class sym_vals{
	public int num_val(){
		throw new IllegalStateException("Error: not an int");
	}
	public String str_val(){
		throw new IllegalStateException("Canot convert to string");
	}
	public sym_vals fn_val(LinkedList<sym_vals> args){
		throw new IllegalStateException("Canot convert to fn: str_val is " + str_val());
	}
	public sym_vals fn_val(LinkedList<sym_vals> args, Env env){
		return fn_val(args);
	}
	
	//the three argument case is used to handle 'let'
	//the default is to call the two arg version, dropping the third arg.
	public sym_vals fn_val(LinkedList<sym_vals> args, Env env, String glyph){
		return fn_val(args,env);
	}
	
	public float float_val(){
		throw new IllegalStateException("Error: not a float");
	}
	public void disp(){
		System.out.println(str_val());
	}
	public boolean bool_val(){
		return true;
	}

  public abstract boolean eq(sym_vals rhs);

}
