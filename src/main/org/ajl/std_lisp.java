package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Iterator;

import java.util.Scanner;

public class std_lisp{
	public Env env = lisp.standard_env();
	public std_parse thep;

	//the_input should be an iterator derived from a scanner with
	//deliimiter "" (matches all).

	public std_lisp(Iterator<String> the_input){
		thep = new std_parse(the_input);
	}

	public std_lisp(){
		thep = new std_parse();
	}
	
	//no throw, use default environment
	//requires expliti i/o
	public String runProg_nothrow(String prog){
		return lisp.runProg_nothrow(prog,env,thep);
	}
	public sym_vals runProg_throw(String prog){
		return lisp.runProg_throw(prog,env,thep);
	}
	
	public String runProg_throw(sym_vals ast){
		return lisp.eval(ast,env).str_val();
	}
}

