package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;

public class print extends fn_vals{ //Makes newline
	public sym_vals fn_val(LinkedList<sym_vals> args){
		for(sym_vals sym : args)
	    print_one(sym);
		return lisp.lisp_true;
	}
	void print_one(sym_vals sym){
		System.out.println(sym.str_val());
	}
}

