package org.ajl;

import java.util.LinkedList;

public class cons extends fn_vals{    
	public sym_vals fn_val(LinkedList<sym_vals> args){
		sym_vals lhs = args.get(0);
		sym_vals rhs = args.get(1);
		sym_list ret;
		if(rhs instanceof sym_list){
	    ret = new sym_list(new LinkedList<sym_vals>(((sym_list)rhs).children));
		}
		else{
	    ret = new sym_list();
	    ret.children.add(rhs);
		}
		ret.children.addFirst(lhs);
	
		return ret;
	}
}
