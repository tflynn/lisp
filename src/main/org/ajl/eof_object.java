package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;

public class eof_object extends fn_vals{//no newline
    public sym_vals fn_val(LinkedList<sym_vals> args){
			if(args.size() != 1)
				throw new IllegalStateException("eof-object expects exactly 1 arg");
			if(args.getFirst() instanceof sym_eof)
				return lisp.lisp_true;

			return lisp.lisp_false;
    }
}
