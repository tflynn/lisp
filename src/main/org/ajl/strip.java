package org.ajl;

import java.util.Iterator;

//should be an iterator derived from a scanner with delimiter "" (matches all).
public class strip  implements Iterator<String>{
	Iterator<String> child;
	
	int QUOTE=1;
	int TOP=0;
	int MODE=TOP;
	
	public strip(Iterator<String> _child){
		child = _child;
	}
  
	public boolean hasNext(){
		return child.hasNext();
	}

	public String next(){
		String nxt = child.next(); //should be a single char
		if(MODE == TOP){
			if(util.is_dquote(nxt.charAt(0))){
				MODE=QUOTE;
			}
			else if(nxt.charAt(0) == ';'){
				until_new_line();
				return "\n";
			}
		}
		else if (MODE == QUOTE){
			if(util.is_dquote(nxt.charAt(0))){
				MODE=TOP;
			}
		}
		return nxt;
	}
	
	public void remove(){
		throw new UnsupportedOperationException("Remove not implemented");
	}

	public static String strip_comments_by_string(String program){
		String ret="";
		int pos=0;
		while(pos < program.length()){
			if(util.is_dquote(program.charAt(pos))){
				ret = ret + program.charAt(pos);
				pos++;
				while(!util.is_dquote(program.charAt(pos))){
					ret = ret + program.charAt(pos);
					pos++;
				}
			}
	    if(program.charAt(pos) != ';'){
				ret = ret + program.charAt(pos);
				pos++;
	    }
	    else{
				pos = util.until_new_line(pos,program);
	    }
		}
		return ret;
	}
		


	public void until_new_line(){
		while( child.hasNext() &&
					 !util.is_new_line(child.next().charAt(0) ));
	}


}
