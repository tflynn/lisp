package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;

//define-syntax handler
public class syntax_defsyn extends syntax_val{ 
	public sym_vals fn_val(LinkedList<sym_vals> vals,
												 Env env){

		ListIterator<sym_vals> listIt = vals.listIterator();
		if(!listIt.hasNext())
	    throw new IllegalStateException
				("define-syntax: missing name");
		sym_vals name = listIt.next(); //name of the syntax def
		if(!(name instanceof sym_data))
	    throw new IllegalStateException
				("define-syntax: invalid form of name");

		if(!listIt.hasNext())
	    throw new IllegalStateException
				("define-syntax: missing parameter name");
		sym_vals parm = listIt.next(); //parameter list of syntax
	
		if(!listIt.hasNext())
	    throw new IllegalStateException
				("define-syntax: missing definition body");
	
		sym_vals val = listIt.next();//body of the syntax definition

		env.put(name.str_val(),
						new syn_procedure(parm,val,env));
	
		return env.get(name.str_val());//define at this level
	}
}
	
