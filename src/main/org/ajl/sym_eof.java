package org.ajl;
//the eof object returned by read when there is no more data.
public class sym_eof extends sym_vals{
    public boolean eq(sym_vals rhs){
			return (rhs instanceof sym_eof);
    }
   
	
    public String str_val(){
			return "(eof)";
    }
	
    boolean val;
}
