package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;

public class display extends fn_vals{//no newline
    public sym_vals fn_val(LinkedList<sym_vals> args){
	for(sym_vals sym : args)
	    display_one(sym);
	return lisp.lisp_true;
    }
    void display_one(sym_vals sym){
	System.out.print(sym.str_val());
    }
}
