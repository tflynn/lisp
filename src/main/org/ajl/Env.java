package org.ajl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.ListIterator;

public class Env extends syntax_val{
	private HashMap<String,sym_vals> the_data =
		new HashMap<String,sym_vals>();

	public sym_vals fn_val(LinkedList<sym_vals> vals,
												 Env env, String mode){ //(env obj)		
		return lisp.eval(vals,this);
	}
	
	public Env(){
   
	}

	public boolean eq(sym_vals rhs){
		return ((Env)rhs).the_data.equals(the_data);
	}
	
	public void putAll(Env rhs){
		the_data.putAll(rhs.the_data);
	}
	
	public Env(Env out){
		outer = out;
	}

	Env is_defined(String s){//returns an env or null
		if(the_data.containsKey(s))
	    return this;
		else if(outer != null)
	    return outer.is_defined(s);
		else
	    return null;
	}
	public void put(String glyph, sym_vals val){
		the_data.put(glyph,val);
	}
	
	Env find(String s){//returns an env or throws
		if(the_data.containsKey(s))
	    return this;
		else if (outer == null)
	    throw new IllegalStateException("Symbol not found: " + s);
		else
	    return outer.find(s);
	}
	sym_vals get(String s){
		//	System.out.println("Get!");
		return the_data.get(s);
	}

	public String str_val(){
		return "<environment>";
	}


	Env outer;
}
