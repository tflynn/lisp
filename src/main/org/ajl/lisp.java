package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Scanner;
//all static vars and fns.
//this class stores no data.
public class lisp{
	public static sym_bool lisp_false = new sym_bool(false);
	public static sym_bool lisp_true = new sym_bool(true);

	public static Env core_env(){ //no i/o
		Env ret = new Env();
		ret.put("++",     new incr());
		ret.put("+",      new plus());
		ret.put("-",      new minus());
		ret.put("*",      new mult());
		ret.put("/",      new div());
		ret.put("<",      new lt());
		ret.put(">",      new gt());
		ret.put("eq?",    new eq());
		ret.put("=",      new eq());
		ret.put("cons",   new cons());
		ret.put("cdr",    new cdr());
		ret.put("car",    new car());
		ret.put("list",   new list());
		ret.put("null?",  new list_null());
		ret.put("random", new random());
		ret.put("list?",  new list_q());
		ret.put("symbol?",new symbol());
		ret.put("begin",  new begin());
		ret.put("exp",    new exp());
		ret.put("log",    new log());
		ret.put("apply",  new apply());
		ret.put("make-string", new make_string());
		ret.put("string-ref", new string_ref());
		ret.put("string-append", new string_append());
		
		ret.put("if",     new syntax_if());
		ret.put("lambda", new syntax_lambda());
		ret.put("define", new syntax_define());
		ret.put("set!",   new syntax_set());
		ret.put("quote",  new syntax_quote());
		ret.put("do",     new syntax_do());
		ret.put("gensym", new gensym());

		
		syntax_let syn_let = new syntax_let();
		ret.put("let",    syn_let);
		ret.put("letrec", syn_let);
		ret.put("let*",   syn_let);

		ret.put("define-syntax", new syntax_defsyn());


		parser stdp = new std_parse();
	    		
		runProg_throw(
									"(define not (lambda (bv) (if bv #f #t)))",
									ret,stdp);

		runProg_throw(
									"(define <= (lambda (a b) (if (= a b) #t (if (< a b) #t #f))))",
									ret,stdp);

		runProg_throw(
									"(define-syntax cond body " +
									"  (let* ((clause1 (car body))" +
									" (rest (cdr body)) " +
									" (cond1 (car clause1))" +
									" (body1 (cdr clause1))) " +
									" (cons (quote if) ;first element is 'if \n" +
									" (cons	   (if (eq? cond1 (quote else)) " +
									" (quote #t) " +
									" cond1) " + 
									" (cons " +
									" (cons ;third element is a begin element \n " +
									" (quote begin)	     body1)" +
									" (if (null? rest)(list) ;cons that with a null elme \n" +
									" (list (cons (quote cond) rest))))))))",
									ret,stdp);

    runProg_throw("(define length (lambda (l) (if (null? l) 0 (+ 1 (length (cdr l))))))",
									ret,stdp);

    runProg_throw("(define cadr (lambda (l) (car (cdr l))))",
									ret,stdp);
    runProg_throw("(define caar (lambda (l) (car (car l))))",
									ret,stdp);
    runProg_throw("(define cdar (lambda (l) (cdr (car l))))",
									ret,stdp);
    runProg_throw("(define cddr (lambda (l) (cdr (cdr l))))",
									ret,stdp);

		runProg_throw("(define caddr (lambda (l) (cadr (cdr l))))",
									ret,stdp);
		runProg_throw("(define cadar (lambda (l)  (car (cdr (car l)))))",
									ret,stdp);
		runProg_throw("(define cdddr (lambda (l)  (cdr (cdr (cdr l)))))",
									ret,stdp);
		runProg_throw("(define caadr (lambda (l)  (car (car (cdr l)))))",
									ret,stdp);

		runProg_throw("(define cadadr (lambda (l) (car (cdr (car (cdr l))))))",
									ret,stdp);

		runProg_throw("(define caaddr (lambda (l) (car (car (cdr (cdr l))))))",
									ret,stdp);

		runProg_throw("(define cdaddr (lambda (l) (cdr (car (cdr (cdr l))))))",
									ret,stdp);

    runProg_throw("" + //note: r5rs stop evaling after first false..
									"(define and " + //that means we should use define-syntax...
									"  (lambda body " +
									"    (if (null? body) " + 
									"	#t " +
									"      (let ((head (car body)) "  +
									"	    (rest (cdr body))) " +
									"	(if (null? rest) ;;last element of list \n" +
									"	    (if head head #f) ;;return head or #f \n" +
									"	  (if head (apply and rest) #f))))));if head rec" ,ret,stdp);

    runProg_throw("" +
									"(define memq " +
									"  (lambda (itm l) " +
									"    (if (null? l) #f " +
									"      (if (eq? (car l) itm) l " +
									"	(memq itm (cdr l)))))) ",ret,stdp);
		
		runProg_throw("" +
									"(define assq " +
									" (lambda (itm l) " +
									"   (if (null? l) " +
									"       #f" +
									"       (if (eq? (caar l) itm)" +
									"           (car l) " +
									"           (assq itm (cdr l))))))",ret,stdp);

    runProg_throw("" +
									"(define map" +
									"  (lambda args" +
									"  (let ((fn (car args)) "  +
									"	(lists (cdr args))) " + 
									"    (if (null? (car lists))" +
									"	(list)" +
									"      (if (eq? (length lists) 1) " +
									"	   (cons (fn (caar lists)) " +
									"		 (map fn (cdr (car lists)))) " +
									"	(cons (apply fn (map car lists)) " +
									"	      (apply map (cons fn (map cdr lists)))))))))" +
									"",ret,stdp);

    runProg_throw("" +
									"(define-syntax which (fn thecond . clauses)" +
									"  (let ((symb (gensym))" +
									"(fnb (gensym)))" +
									"(list " +
									"(quote let) " +
									"(list (list symb thecond)" +
									"(list fnb fn))" +
									"(cons (quote cond) " +
									"(map (lambda (clause) " +
									"(let ((cset (car clause))) " +
									"(cons (if (eq? cset (quote else)) " +
									"(quote else) " +
									"(list fnb symb cset)) " +
									"(cdr clause)))) " +
									"clauses)))))",ret,stdp);

    runProg_throw("" +
									"(define-syntax case body" +
									" (push-front (quote which) (quote memq) body ))" +
									"",ret,stdp);


    runProg_throw("" +
									"(define append " +
									"(lambda body " +
									" (if (null? body) body " +
									"  (let ((a (car body)) " +
									"(tail (cdr body))) " +
									"(if (null? tail) a " +
									"(if (null? a) " +
									"(apply append tail) " +
									"(push-front " +
									"(car a) " +
									"(apply " +
									"append " +
									"(push-front (cdr a) tail))))))))) " +
									"",ret,stdp);

		runProg_throw("" +
								 "(define reverse (lambda (l)" +
								 "  (if (null? l) l" +
								 "      (append (reverse (cdr l))" +
								 "              (list (car l))))))" +
								 "",ret,stdp);

    runProg_throw("" +
									"(define push-front" +
									"  (lambda body" +
									"    (if (eq? (length body) 1)" +
									"        (car body)" +
									"        (let ((head (car body)) " +
									"              (tail (cdr body)))" +
									"          (cons head (apply push-front tail))))))" +
									"",ret,stdp);
    
    runProg_throw("" +
									"    (define push-back" +
									"  (lambda body " +
									"    (let ((head (car body))" +
									"	  (tail (cdr body)))" +
									"	  (append head tail))))" +
									"",ret,stdp);
		/*
			; flatten a list
			; e.g. 
			; (flatten (a (b c) (d (e f (g))) h )) =>
			; (a b c d e f g h)
		*/
		runProg_throw(""+
									"(define flatten" +
									"  (lambda (l)" +
									"    (if (null? l) " +
									"         l" +
									"         (if (not (list? l))" +
									"             l" +
									"             (if (not (null? car l))" +
									"                 (if (not (list? (car l)))" +
									"                     (cons (car l) (flatten (cdr l)))" +
									"                     (append (flatten (car l))" +
									"                             (flatten (cdr l)))))))))",
									ret,stdp);
		runProg_throw("" +
									"		(define equal? (lambda (l1 l2)" +
									"		 (if (and (list? l1) (list? l2))" +
									"		     (if (eq? (length l1) (length l2))" +
									"			 (if (null? l1) #t" +
									"			   (and" +
									"			    (equal? (car l1) (car l2))" +
									"			    (equal? (cdr l1) (cdr l2))))" +
									"		       #f)" +
									"		   (eq? l1 l2))))",
									ret,stdp);

    runProg_throw("" + //member: like memq, but comparisons use equal?, not eq?
									"(define member " +
									"  (lambda (itm l) " +
									"    (if (null? l) #f " +
									"      (if (equal? (car l) itm) l " +
									"	(member itm (cdr l)))))) ",ret,stdp);
		
		return ret;
	}

	//core environment + console i/o + file i/o
	public static Env standard_env(){ 
		//Make a std environment
		Env ret = core_env();
		ret.put("input",      new input());
		ret.put("display",    new display());
		ret.put("print",      new print());
		ret.put("load",       new load());
		ret.put("merge",      new merge());
		ret.put("newline",    new newline());
		ret.put("read",       new read());
		ret.put("eof-object?",new eof_object());
		
		return ret;
	}

	public static sym_vals getNumeric(String str){
		try{
	    int x = Integer.parseInt(str,10);
	    return new sym_num(x);
		}
		catch(Throwable e){
	    try{
				float y = Float.parseFloat(str);
				return new sym_float(y);
	    }
	    catch(Throwable q){
				return null;
	    }
		}
	}

	public static sym_vals getBool(String str){
		if(str.equals("#f"))
	    return lisp_false;
		else if(str.equals("#t"))
	    return lisp_true;
		else
	    return null;
	}

	public static sym_str getString(String str){
		if((str.charAt(0) == '"') &&
			 (str.charAt(str.length() - 1) == '"'))
	    return new sym_str(str.substring(1,str.length()-1));
		return null;
	}

	public static sym_vals getData(String glyph){
		sym_vals ret;
		if     (! ((ret = getNumeric(glyph)) == null));
		else if(! ((ret = getBool(glyph)) == null));
		else if(! ((ret = getString(glyph)) == null));
		else;
		
		return ret;
	}
	
	public static sym_vals eval(LinkedList<sym_vals> asts, Env env){
		sym_vals cur=null;
		for(sym_vals ast : asts)
	    cur=eval(ast,env);
	
		return cur;
	}
    
	public static sym_vals eval(sym_vals ast, Env env){//,fn){
		//System.out.println("evalling teh ast");
		try{
	    if( ast instanceof sym_data ) {//can be name, num, bool, string..
				String glyph = ((sym_data)ast).glyph;
				sym_vals ret;
				if( (ret = getData(glyph)) == null)  	//datarep of number?
					ret = env.find(glyph).get(glyph); //fn(env.find(gly,gly)

				return ret;
	    }
	    else if (ast instanceof sym_list){ //fn call/special form
				//System.out.println("Returning teh specialz");

				sym_list ast_in = (sym_list) ast;
				if (ast_in.children.size() == 0)  return lisp_false;
       
				ListIterator<sym_vals> listIt = ast_in.children.listIterator();
				sym_vals first_child = listIt.next();

				sym_vals fn = eval( first_child,env ); 

				LinkedList<sym_vals> params = new LinkedList<sym_vals>();

				if( fn instanceof syntax_val){//for the syntax, don't eval first
					while(listIt.hasNext())
						params.add(listIt.next());
				}
				else{//normal procedure - eval all parameters first
					while(listIt.hasNext())
						params.add( eval( listIt.next() , env ) );
				}
				return fn.fn_val(params,env,first_child.str_val());
				//third argument is name of fn/mac.
				//used to differentiate let/letrec etc.
	    }
	    else{ //what would this be???
				//System.out.println("Returning teh astz");
				return ast;
	    }
		}catch(Throwable e){
			//System.out.println("Catchin teh throwablz");

	    int line=-1;
	    if  (ast instanceof sym_list)
				line = ((sym_list)ast).line;
	    else if (ast instanceof sym_data)
				line = ((sym_data)ast).line;
	    if(line != -1)
				throw new IllegalStateException("Near line " + line + " :\n" + e.getMessage());
	    else
				throw e;
		}
	
	}

	//runprog, throwing on internal error.
	//this is called by runprog_nothrow, defined below.
	public static sym_vals runProg_throw(String prog, Env env,
																			 parser p) {
		return eval(p.parse(prog),env);
	}


	//no throw: appends error to return string. else eval result
	//returns final result of eval -> to_str()
	//this calls runprog_throw, defined above.
	public static String runProg_nothrow(String prog, Env env,parser thep){
		//System.out.println("Runnin teh prog");
		String res = "";
		try{
			sym_vals rv;
			rv = runProg_throw(prog,env,thep);
			if(!(rv == null)){
				return rv.str_val();
			}
			else{
				//System.out.println("Nullity");
				return "";
			}
		}
		catch(Exception e){
	    res = "Error: " + e.getMessage();
		}
		return res;
	}


}
