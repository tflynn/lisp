package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;

public class syntax_quote extends syntax_val{
	public sym_vals fn_val(LinkedList<sym_vals> vals,
												 Env env){

		ListIterator<sym_vals> listIt = vals.listIterator();
		if(!listIt.hasNext())
	    throw new IllegalStateException("Missing quote body");
		return listIt.next();
	}
    
}
