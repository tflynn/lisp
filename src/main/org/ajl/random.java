package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Random;

public class random extends fn_vals{//no newline
    public sym_vals fn_val(LinkedList<sym_vals> args){
	String str="";
	Object first = args.getFirst();
	if(first instanceof sym_num){
	    sym_num max_num = (sym_num)first;
	    return new sym_num(r.nextInt(max_num.num_val()));
	}
	else if (first instanceof sym_float){
	    sym_float max_num = (sym_float)first;
	    return new sym_float(r.nextFloat()*max_num.float_val());
	}
	throw new IllegalStateException("random expected float or int argument");
    }
    Random r = new Random();
}
