package org.ajl;

import java.util.ListIterator;
import java.util.LinkedList;

public class sym_list extends sym_vals{
	public sym_list(LinkedList<sym_vals> _children, int _line){
		line = _line;
		children = _children;
	}
	public sym_list(LinkedList<sym_vals> _children){
		this(_children,-1);
	}
	public sym_list(){
		this(new LinkedList<sym_vals>());
	}
	public boolean eq(sym_vals rhs){
		return false;
	}
	public String str_val(){
		String ret;
		ret = "( ";
		ListIterator<sym_vals> listIt = children.listIterator();
		while(listIt.hasNext())
	    ret += listIt.next().str_val() + " ";
		ret += ")";
		return ret;
	
	}

	int line;
	LinkedList<sym_vals> children = new LinkedList<sym_vals>();
}
