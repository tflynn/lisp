package org.ajl;

public class sym_num extends sym_vals{
	public boolean eq(sym_vals rhs){
		return ((sym_num)rhs).val == val;
	}
	public sym_num(int _val){
		val = _val;
	}
	public int num_val(){ //numeric value of a symbol
		return val;
	}
	public float float_val(){
		return val;
	}
	public String str_val(){
		return Integer.toString(val);
	}

	int val;
}
