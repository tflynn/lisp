package org.ajl;

import java.util.LinkedList;

public class gensym extends fn_vals{  //for now, true if symdata
    public gensym(){
	counter = 0;
    }
    public sym_vals fn_val(LinkedList<sym_vals> args){
	sym_data ret = new sym_data("#:" + counter);
	counter=counter+1;
	return ret;
    }
    
    int counter;
}
