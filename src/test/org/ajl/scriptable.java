import org.ajl.fn_vals;
import org.ajl.std_lisp;
import org.ajl.sym_vals;
import org.ajl.sym_str;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

class date extends fn_vals{
	public sym_vals fn_val(LinkedList<sym_vals> args){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date now = new Date();
		String strDate = sdf.format(now);
		return new sym_str(strDate);
	}
	
}

public class scriptable {

	public static void main(String argv[]) {
		std_lisp mylisp = new std_lisp();
		mylisp.env.put("date",new date());

		String prog =
			"(print \"Hello world. Here is the current date and time: \") " +
			"(print (date))" + 
			"(print \"Have a nice day!\")";
		mylisp.runProg_nothrow(prog);
	}
}
